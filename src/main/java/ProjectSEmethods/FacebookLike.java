package ProjectSEmethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import Wdmethods.SeMethods;

public class FacebookLike extends SeMethods {
	
	@Test
	public void like()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver;
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);

		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		WebElement username = driver.findElementById("email");
		username.sendKeys("vetri.velan1991@gmail.com");
		WebElement password = driver.findElementById("pass");
		password.sendKeys("");
		WebElement login = driver.findElementByXPath("//input[@type='submit']");
		login.click();
		WebElement search = driver.findElementByXPath("//input[@role='combobox']");
		search.sendKeys("TestLeaf");
		WebElement searchClick = driver.findElementByXPath("//button[@type='submit']");
		searchClick.click();
		WebElement heading = driver.findElementByXPath("//div[text()='TestLeaf']");
		verifyExactText(heading, "TestLeaf");
		WebElement likeButton = driver.findElementByXPath("(//button[@type='submit'])[4]");
		String likeText = likeButton.getText();
		if(likeText.equalsIgnoreCase("Like"))
		{
			likeButton.click();
		}
		else
		{
			System.out.println("Page is already Liked");
		}
		
		heading.click();
		WebElement title = driver.findElementByXPath("//span[text()='TestLeaf']");
		//String titleText = title.getText();
		verifyExactText(title, "TestLeaf");
		WebElement likesCount = driver.findElementByXPath("(//div[@class='clearfix _ikh'])[2]/div[2]/div");
		String likeCountText = likesCount.getText().replaceAll("//D", "");
		System.out.println("Number of Likes "+likeCountText);
		
	}

}
