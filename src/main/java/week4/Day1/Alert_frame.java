package week4.Day1;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alert_frame {

	public static void main(String[] args) {
	
System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
ChromeDriver driver=new ChromeDriver();
driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
driver.switchTo().frame("iframeResult");
driver.findElementByXPath("//button[text()='Try it']").click();
Alert Palert = driver.switchTo().alert();
String alerttext = Palert.getText();
Palert.sendKeys("Maha");
Palert.accept();
System.out.println("Hello Maha! How are you today?");
driver.switchTo().defaultContent();


	}

}